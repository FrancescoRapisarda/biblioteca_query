/*
	CREARE UN SISTEMA DI GESTONE LIBRI;
    OGNI LIBRO SARA' CARATTERIZZATO DA:
    - Titolo
    - Autore
    - Anno di pubblicazione (intero)
    - Codice ISBN (univoco)
    
    
    Effetturare le seguenti operazioni:
    - Modifica la tabella ed inserisci la casa editrice
    - Inserire almeno 6 libri con dati diversi
    - Cercare tutti i libri di un dato autore
    - Cercare tutti i libri che abbiano un certo anno di pubblicazione
    - Modificare un libro tramite il codice ISBN
    - Guarda cosa succede se aggiungo una nuova colonna che si chiama "Prezzo di Vendita"
    
*/

CREATE DATABASE Biblioteca;

USE Biblioteca;

SHOW DATABASES;

CREATE TABLE Libro (
		Titolo VARCHAR(40),
        Autore VARCHAR(60),
        Anno_Publicazione INTEGER,
        Cod_ISBN INTEGER PRIMARY KEY
		);
        
SHOW TABLES;

-- Modifico il tipo nel campo "cod_isbn" della tabella "libro"
ALTER TABLE libro MODIFY COLUMN cod_isbn VARCHAR(17);


-- (2) Popolo i campi della tabella
INSERT INTO libro
VALUES("Cose che nessuno sa","Alessandro D'Avenia", 2011, "187-495-23-868-23"),
("L'Idiota","F. M. Dostoevskij", 1869, "165-495-23-868-23"),
("L'Allegria degli angoli","Marco Presta", 2014, "185-495-23-868-23"),
("Uno nessuno e centomila","Luigi Pirandello", 1926, "155-495-23-868-23"),
("La Fattoria degli animali","George Orwell", 1945, "196-495-23-868-23"),
("L'Amica geniale","Elena Ferrante", 2011, "175-495-23-868-23");


-- (2) Popolo i campi della tabella
INSERT INTO libro
VALUE ("Cose che nessuno sa","Alessandro D'Avenia", 2011, "145-495-23-868-23");

SELECT * FROM libro;

DELETE FROM libro WHERE cod_isbn = "187-495-23-868-23";


-- (1) Aggiungo "Casa editrice"
ALTER TABLE libro ADD COLUMN Casa_editrice VARCHAR(50);


-- (3) Cerca libro per autore
SELECT * FROM libro
WHERE autore = "F. M. Dostoevskij";


-- (4) Cerca tutti i libri che abbiano un certo anno di pubblicazione
SELECT * FROM libro
WHERE anno_Publicazione = 2011;


-- (5) Modifica un libro tramite il codice ISBN
UPDATE libro SET autore = "E. Ferrante"
WHERE cod_isbn = "175-495-23-868-23";

SELECT * FROM libro;


-- (6) Cosa succede se aggiungo una nuova colonna che si chiama "Prezzo di Vendita"?
ALTER TABLE libro ADD COLUMN Prezzo_di_vendita FLOAT;

SELECT * FROM libro;


-- MODIFICHE PERSONALI OLTRE LA TASK DI GIOVANNI --


-- Set casa editrice "libro L'Idiota"
UPDATE libro SET casa_editrice = "Mondatori"
WHERE cod_isbn = "165-495-23-868-23";

SELECT * FROM libro;


-- Set prezzo di vendita "libro La Fattoria degli animali"
UPDATE libro SET prezzo_di_vendita = "19.90"
WHERE cod_isbn = "196-495-23-868-23";

SELECT * FROM libro;